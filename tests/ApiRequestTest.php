<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiRequestTest extends WebTestCase
{
    public function testGetQuestionsByVotes(): void
    {
        $client = static::createClient();
        $client->request(
            'GET',
            '/api/show-by-votes',
        );

        echo $client->getResponse();
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testNewQuestion(): void
    {
        $client = static::createClient();
        $client->request(
            method: 'POST',
            uri: '/api/question/new',
            server: ['Content-Type' => 'application/json'],
            content: '{"name":"Náhodná pracovní otázka č. '.rand(0,100).'", "text":"Náhodný problém v práci č.'.rand(1,15).'"}'
        );

        echo $client->getResponse();
        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }

    public function testNewQuestionFailure(): void
    {
        $client = static::createClient();
        $client->request(
            method: 'POST',
            uri: '/api/question/new',
            server: ['Content-Type' => 'application/json'],
//            content: '{"name":"Náhodná pracovní otázka č. '.rand(0,100).'", "text":"Náhodný problém v práci č.'.rand(1,15).'"}'
        );

        echo $client->getResponse();
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }
}
