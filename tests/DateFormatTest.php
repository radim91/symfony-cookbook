<?php

namespace App\Tests;

use App\Entity\Question;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DateFormatTest extends KernelTestCase
{
    public function testDatetimeVersusString(): void
    {
        $this->expectException('TypeError');

        $question = new Question();
        $question->setDate('abcdefg');

    }

    public function testDatetimeVersusInteger(): void
    {
        $this->expectException('TypeError');

        $question = new Question();
        $question->setDate(123456);
    }

    public function testDatetimeVersusDatetime(): void
    {
        $this->expectException('TypeError');

        $question = new Question();
        $question->setDate(new \DateTime());
    }
}
