<?php

namespace App\MessageHandler;

use App\Message\SendEmailMessage;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Mime\Email;

final class SendEmailMessageHandler implements MessageHandlerInterface
{
    public function __construct(
        private MailerInterface $mailer,
        private string $adminMail
    ) {}

    public function __invoke(SendEmailMessage $message)
    {
        $email = (new Email())
                ->from('aplikace@inventi-overflow.cz')
                ->to($this->adminMail)
                ->subject('Nový dotaz z Inventi Overflow')
                ->text('Přibyl nový dotaz č. '.$message->getId())
            ;

        $this->mailer->send($email);
    }
}
