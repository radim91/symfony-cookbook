<?php

namespace App\DataFixtures;

use App\Entity\Question;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 10; $i++) {
            $question = new Question();
            $question->setName('Důležitá pracovní otázka '.rand(1,1000));
            $question->setText('Jak si mám nejlépe vybrat z benefitů č. '.rand(1,20));
            $question->setVotes(rand(-20, 50));
            $question->setDate(new \DateTime());

            $manager->persist($question);
        }

        $manager->flush();
    }
}
