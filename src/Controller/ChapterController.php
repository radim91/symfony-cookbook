<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ChapterController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function home(): Response
    {
        return $this->render('chapter/home.html.twig');
    }

    #[Route('/01-jak-zacit', name: 'one')]
    public function chapterOne(): Response
    {
        return $this->render('chapter/one.html.twig', [
            'header' => 'Jak začít? Symfony CLI!'
        ]);
    }

    #[Route('/02-hello-world', name: 'two')]
    public function chapterTwo(): Response
    {
        return $this->render('chapter/two.html.twig', [
            'header' => 'Hello World'
        ]);
    }

    #[Route('/03-dockerbaze', name: 'three')]
    public function chapterThree(): Response
    {
        return $this->render('chapter/three.html.twig', [
            'header' => 'Dockerbáze'
        ]);
    }

    #[Route('/04-zkusime-modelling', name: 'four')]
    public function chapterFour(): Response
    {
        return $this->render('chapter/four.html.twig', [
            'header' => 'Zkusíme modelling?'
        ]);
    }

    #[Route('/05-plnime-databazi', name: 'five')]
    public function chapterFive(): Response
    {
        return $this->render('chapter/five.html.twig', [
            'header' => 'Plníme databázi'
        ]);
    }

    #[Route('/06-a-vybirame-z-ni', name: 'six')]
    public function chapterSix(): Response
    {
        return $this->render('chapter/six.html.twig', [
            'header' => 'A vybíráme z ní'
        ]);
    }

    #[Route('/07-jdeme-volit', name: 'seven')]
    public function chapterSeven(): Response
    {
        return $this->render('chapter/seven.html.twig', [
            'header' => 'Jdeme volit'
        ]);
    }

    #[Route('/08-uzivateli-napis-nam', name: 'eight')]
    public function chapterEight(): Response
    {
        return $this->render('chapter/eight.html.twig', [
           'header' => 'Uživateli, napiš nám!'
        ]);
    }

    #[Route('/09-upozorneni-mailem', name: 'nine')]
    public function chapterNine(string $adminMail): Response
    {
        return $this->render('chapter/nine.html.twig', [
           'header' => 'Upozornění mailem',
           'adminMail' => $adminMail
        ]);
    }

    #[Route('/10-async-s-rabbitmq', name: 'ten')]
    public function chapterTen(): Response
    {
        return $this->render('chapter/ten.html.twig', [
            'header' => 'Async s RabbitMQ'
        ]);
    }

    #[Route('/11-testing', name: 'eleven')]
    public function chapterEleven()
    {
        return $this->render('chapter/eleven.html.twig', [
            'header' => 'Testing'
        ]);
    }

    #[Route('/12-funkcionalni-testy', name: 'twelve')]
    public function chapterTwelve()
    {
        return $this->render('chapter/twelve.html.twig', [
            'header' => 'Funkcionální testy API'
        ]);
    }
}
