<?php

namespace App\Controller;

use App\Entity\Question;
use App\Form\NewQuestionType;
use App\Message\SendEmailMessage;
use App\Repository\QuestionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    #[Route('/main', name: 'main')]
    public function main(): Response
    {
        return $this->render('main/index.html.twig', [
            'msg' => 'Hello World!'
        ]);
    }
    
    #[Route('/new', name: 'new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager, MessageBusInterface $bus): Response|RedirectResponse
    {
        $question = new Question();

        $form = $this->createForm(NewQuestionType::class, $question);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $question->setDate(new \DateTime());
            $question->setVotes(0);

            $entityManager->persist($question);
            $entityManager->flush();

            $bus->dispatch(new SendEmailMessage($question));

            $this->addFlash('success', 'Dotaz byl úspěšně přijat');
            return $this->redirectToRoute('list');
        }

        return $this->render('main/new.html.twig', [
           'form' => $form->createView(),
        ]);
    }

    #[Route('/new-random', name: 'new-random')]
    public function newRandom(EntityManagerInterface $entityManager): RedirectResponse
    {
        $question = new Question();
        $question->setName('Důležitá pracovní otázka '.rand(1,1000));
        $question->setText('Jak si mám nejlépe vybrat z benefitů č. '.rand(1,20));
        $question->setVotes(rand(-20, 50));
        $question->setDate(new \DateTime());

        $entityManager->persist($question);
        $entityManager->flush();

        return $this->redirectToRoute('main');
    }

    #[Route('/show-by-votes', name: 'show-by-votes')]
    public function showByVotes(QuestionRepository $questionRepository): Response
    {
        return $this->render('main/showByVotes.html.twig', [
            'questions' => $questionRepository->findByVotes(10)
        ]);
    }

    //místo indexu, který je v tutoriálu - na cestě / je nabindován seznam kapitol pro kuchařku
    #[Route('/list', name: 'list')]
    public function list(QuestionRepository $questionRepository)
    {
        $questions = $questionRepository->findAllAndOrderByDESC(10);

        return $this->render('main/list.html.twig', [
            'questions' => $questions
        ]);
    }

    #[Route('/vote/{id}', name: 'vote', methods: ['POST'])]
    public function vote(Question $question, Request $request, EntityManagerInterface $entityManager): RedirectResponse
    {
        $direction = $request->request->get('direction');

        if ($direction === 'up') {
            $question->upVote();
        } elseif ($direction === 'down') {
            $question->downVote();
        }

        $entityManager->flush();

        return $this->redirectToRoute('list');
    }
}
