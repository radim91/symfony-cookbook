<?php

namespace App\Controller;

use App\Entity\Question;
use App\Repository\QuestionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    #[Route('/api/main', name: 'api.main', methods: ['GET'])]
    public function index(): JsonResponse
    {
        return new JsonResponse(['msg' => 'Hello World!']);
    }

    #[Route('/api/show-by-votes', name: 'api.show-by-votes', methods: ['GET'])]
    public function showByVotes(QuestionRepository $questionRepository): JsonResponse
    {
        return new JsonResponse($questionRepository->findByVotes(10));
    }

    #[Route('/api/vote/{id}/{direction}', name: 'api.vote', methods: ['PUT'])]
    public function vote(string $direction, Question $question, EntityManagerInterface $entityManager): JsonResponse
    {
        if ($direction === 'up') {
            $question->upVote();
            $msg = 'Úspěšně jsi upvotoval';
        } elseif ($direction === 'down') {
            $question->downVote();
            $msg = 'Úspěšně jsi downvotoval';
        } else {
            return new JsonResponse(['msg' => 'Chyba, jde upvotovat nebo downvotovat. Tohle nedává smysl'], 400);
        }

        $entityManager->flush();

        return new JsonResponse(['msg' => $msg], 200);
    }

    #[Route('/api/question/new', name: 'api.question.new', methods: ['POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): JsonResponse
    {
        if($request->getContent() === "") {
            return new JsonResponse(
                [
                    'type' => $this->generateUrl('api.question.new'),
                    'title' => 'Request je null.',
                    'status' => 400,
                    'detail' => 'Poslaný request neobsahuje žádná data'
                ],
                400,
                ['Content-Type' => 'application/problem+json']
            );
        } else {
            $data = json_decode($request->getContent(), false);
            $question = new Question();

            $question->setName($data->name);
            $question->setText($data->text);
            $question->setVotes(0);
            $question->setDate(new \DateTime());

            $entityManager->persist($question);
            $entityManager->flush();

            return new JsonResponse(
                ['success' => 'Dotaz byl úspěšně vytvořen.'],
                201,
                [
                    'Location' => '/api/question/'.$question->getId(),
                    'Content-Type' => 'application/json'
                ]
            );
        }
    }
}
