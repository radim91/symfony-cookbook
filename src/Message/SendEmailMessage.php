<?php

namespace App\Message;

use App\Entity\Question;

final class SendEmailMessage
{
    public function __construct(
        private Question $question
    ) {}

    public function getId(): int
    {
        return $this->question->getId();
    }
}
